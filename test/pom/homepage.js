
var Page = require('./page');
var sleep =require('sleep');

var HomePage = Object.create(Page, {
    signupLink: { get: function () { return browser.element('#NAVBAR_SIGNUP_BTN'); } },

    signupEmailaddress: { get: function () { return browser.element('//input[@name="email"]'); } },
    signupPassword: { get: function () { return browser.element('input[name="password"]'); } },
    signupConfirmpassword: { get: function () { return browser.element('input[name="confirmPassword"]'); } },
    signupSignupSubmit: { get: function () { return browser.element('div=Sign Up'); } },

    accountName: {get: function(){return browser.element('a#accountPreviewInner span')}},

    loginLink: {get: function(){return browser.element('a#NAVBAR_SIGNIN_BTN span')}},
    loginEmailaddress: { get: function () { return browser.element('input[name="email"]'); } },
    loginPassword: { get: function () { return browser.element('input[name="password"]'); } },
    loginSubmit: { get: function () { return browser.element('div=Log In'); } },

    logoutButton: { get: function () { return browser.element('#NAVBAR_SIGNOUT_BTN'); } },


    onsaleLink: {get: function(){return browser.element('a*=On Sale!')}},
    freshProduceLink: {get: function(){return browser.element('a[href="/fresh-produce"]')}},
    marketPlaceLink: {get: function(){return browser.element('a[href="/marketplace"]')}},

    cartValueLabel: {get: function(){return browser.element('.NavbarCartTotal__container___15sws')}},
    viewMyCartLink: {get: function(){return browser.element('a.donothide')}},





    open: { value: function() {
        Page.open.call(this, '/');
    } },



    getAccountName: {value : function(){
        var i=5;
        do{
            sleep.msleep(5000);
            if (this.accountName.isVisible()) {
                var text=this.accountName.getText();
                console.log(text);
                return (text);

            }
        }while(--i>0)
    }},

    accountDropdown:{value : function(){
        browser.moveToObject('a#accountPreviewInner');

    }

    },

    close: {value: function(){
        Page.close.call(this);
    }

    }


});

module.exports=HomePage;