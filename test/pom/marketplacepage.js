var Page = require('./page');
var sleep =require('sleep');

var MarketPlacePage = Object.create(Page, {
    brand1: {get: function(){ return(browser.element('#marketplaceTile5'))}},
    mfirstItem: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(1) div.controls div a'))}},
    firstItemPriceLabel: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(1) span[itemprop="price"]'))}},
    mfirstItemCounter: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(1) .AtcButton__counter___iR7_X'))}},

    msecondItem: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(2) div.controls div a'))}},
    secondItemPriceLabel: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(2) span[itemprop="price"]'))}},
    msecondItemCounter: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(2) .AtcButton__counter___iR7_X'))}},

    mthirdItem: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(3) div.controls div a'))}},
    thirdItemPriceLabel: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(3) span[itemprop="price"]'))}},
    mthirdItemCounter: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(3) .AtcButton__counter___iR7_X'))}},
});

module.exports=MarketPlacePage;