
var Page = require('./page');
var sleep =require('sleep');

var CartPage = Object.create(Page, {
    secondItemLabel: { get: function () { return browser.element('div.productList div.productContainer:nth-child(2) h4 a'); } },
    secondItemSubtotal: { get: function () { return browser.element('div.productList div.productContainer:nth-child(2) .subtotal'); } },
    secondItemRemoveButton: { get: function () { return browser.element('div.productList div.productContainer:nth-child(2) .remove'); } },
    grandTotalLabel: { get: function () { return browser.element('div.total'); } },

    checkoutButton: { get: function () { return browser.element('.orderSummaryCheckoutBtn'); } },



});

module.exports=CartPage;

