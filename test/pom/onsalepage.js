var Page = require('./page');
var sleep =require('sleep');

var OnSalepage = Object.create(Page, {
    firstItem: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(1) div.controls div a'))}},
    firstItemPriceLabel: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(1) span[itemprop="price"]'))}},
    firstItemCounter: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(1) .AtcButton__counter___iR7_X'))}},

    secondItem: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(2) div.controls div a'))}},
    secondItemPriceLabel: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(2) span[itemprop="price"]'))}},
    secondItemCounter: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(2) .AtcButton__counter___iR7_X'))}},

    thirdItem: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(3) div.controls div a'))}},
    thirdItemPriceLabel: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(3) span[itemprop="price"]'))}},
    thridItemCounter: {get: function(){ return(browser.element('ul.productList.columnContainer.page-type-undefined li:nth-child(3) .AtcButton__counter___iR7_X'))}}
});


module.exports=OnSalepage;