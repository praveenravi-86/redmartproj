
var Page = require('./page');
var sleep =require('sleep');

var CheckoutPage = Object.create(Page, {
    addAddressButton: { get: function () { return browser.element('.addBtn.editMode'); } },
    postalCodeInput: { get: function () { return browser.element('#ADDRESS_WIZARD_SUGGESTIONS_INPUT'); } },
    cantfindaddress: { get: function () { return browser.element('//div[contains(text(),"Can\'t find your address?")]'); } },


    firstNameInput: { get: function () { return browser.element('.editDeliveryAddress input[name="first_name"]'); } },
    lastNameInput: { get: function () { return browser.element('.editDeliveryAddress input[name="last_name"]'); } },
    streetInput: { get: function () { return browser.element('.editDeliveryAddress input[name="street"]'); } },
    buildingNoInput: { get: function () { return browser.element('.editDeliveryAddress input[name="building_no"]'); } },
    floorInput: { get: function () { return browser.element('.editDeliveryAddress input[name="floor"]'); } },
    unitNumberInput: { get: function () { return browser.element('.editDeliveryAddress input[name="unit_number"]'); } },
    manualPostalCodeInput: { get: function () { return browser.element('.editDeliveryAddress input[name="postcode"]'); } },
    saveAddressButton: { get: function () { return browser.element('.editDeliveryAddress .button.closeEditMode.saveManualAddress'); } },

    contactNumberInput: { get: function () { return browser.element('#contactNumber'); } },

    selectAddressButton: { get: function () { return browser.element('.button.selectSearchedAddress'); } },

    nextButton: { get: function () { return browser.element('.button.nextBtn'); } },


    deliverySlotButton: { get: function () { return browser.element('div.weekday:nth-child(1) .timeslots .slot.incentive.unavailableItems'); } },

    addressNameLabel: { get: function () { return browser.element('.address.selected span'); } },
    addressPostalCodeLabel: { get: function () { return browser.element('.address.selected div.postcode'); } },



});

module.exports=CheckoutPage;
