/**
 * Created by 16733 on 03/08/18.
 */
function Page () {
    this.title = 'My Page';
}

Page.prototype.open = function (path) {
    browser.timeouts('pageload',30000);
    browser.url(path);

}

Page.prototype.close= function(){
    browser.end();
}

module.exports = new Page();