
var assert = require('assert');
var sleep = require('sleep');
var testdata = require('./data/testdata');
var homepage = require('./pom/homepage');
var marketplacepage = require('./pom/marketplacepage');
var freshproducepage = require('./pom/freshproducepage');
var onsalepage = require('./pom/onsalepage');
var cartpage = require('./pom/cartpage');
var checkoutpage = require('./pom/checkoutpage');
var helper = require('./helpers/helper');






describe('Sign up', function() {
    it('should be able to signup', function  (done) {
        homepage.open();
        console.log('Page loaded');
        homepage.signupLink.click();
        testdata.username=testdata.username+(Math.floor(Math.random() * 10000) + 100)+"@gmail.com"
        homepage.signupEmailaddress.setValue(testdata.username);
        homepage.signupPassword.setValue(testdata.password);
        homepage.signupConfirmpassword.setValue(testdata.password);
        homepage.signupSignupSubmit.click();
        //sleep.msleep(8000);
        //console.log(homepage.accountName.getText());

        var accountname=homepage.getAccountName();
        assert.equal(accountname,"Hi, "+ (testdata.username.substr(0,testdata.username.indexOf('@'))));
        helper.logout();
        done;

    });
});


describe('Login', function() {
    it('should be able to login', function  (done) {

        helper.login();

        var accountname=homepage.getAccountName();
        assert.equal(accountname,"Hi, "+ (testdata.username.substr(0,testdata.username.indexOf('@'))));
        //helper.logout();
        done;

    });
});


describe('Shopping test', function() {
    it('should be able to add to cart', function  (done) {


        helper.cartValue=0;
        helper.updatedCartValue=parseFloat(homepage.cartValueLabel.getText().slice(1));

        if(isNaN(helper.updatedCartValue)){
            helper.updatedCartValue=0;
        }else{
            helper.cartValue=helper.updatedCartValue;
        }

        helper.retryclick(homepage.marketPlaceLink,homepage.marketPlaceLink);
        //Add 3 items from market place
        helper.retryclick(marketplacepage.brand1,marketplacepage.mfirstItem);

        helper.addToCart(marketplacepage.mfirstItem,marketplacepage.firstItemPriceLabel,marketplacepage.mfirstItemCounter);

        helper.addToCart(marketplacepage.msecondItem,marketplacepage.secondItemPriceLabel,marketplacepage.msecondItemCounter);

        helper.addToCart(marketplacepage.mthirdItem,marketplacepage.thirdItemPriceLabel,marketplacepage.mthirdItemCounter);

        //Add 3 items from fresh produce
        homepage.freshProduceLink.click();
        freshproducepage.firstItem.waitForVisible(10000);

        helper.addToCart(freshproducepage.firstItem,freshproducepage.firstItemPriceLabel,freshproducepage.firstItemCounter);

        helper.addToCart(freshproducepage.secondItem,freshproducepage.secondItemPriceLabel,freshproducepage.secondItemCounter);

        helper.addToCart(freshproducepage.thirdItem,freshproducepage.thirdItemPriceLabel,freshproducepage.thridItemCounter);

        //Add 3 items from On Sale
        homepage.onsaleLink.click();
        onsalepage.firstItem.waitForVisible(10000);

        helper.addToCart(onsalepage.firstItem,onsalepage.firstItemPriceLabel,onsalepage.firstItemCounter);

        helper.addToCart(onsalepage.secondItem,onsalepage.secondItemPriceLabel,onsalepage.secondItemCounter);

        helper.addToCart(onsalepage.thirdItem,onsalepage.thirdItemPriceLabel,onsalepage.thridItemCounter);


        //logout();
        done;

    });
});

describe('Remove from cart', function() {
    it('should be able to add to cart', function  (done) {

        //login();

        homepage.cartValueLabel.waitForEnabled(10000);

        homepage.cartValueLabel.click();

        cartpage.secondItemRemoveButton.waitForVisible(10000);
        helper.removeFromCart(cartpage.secondItemRemoveButton,cartpage.secondItemSubtotal,cartpage.secondItemLabel);



        //logout();
        done;

    });
});



describe('Checkout', function() {
    it('should be able to checkout', function  (done) {

        //login();

        //homepage.cartValueLabel.click();

        //cartpage.checkoutButton.waitForVisible(20000);
        cartpage.checkoutButton.click();
        checkoutpage.addAddressButton.waitForVisible(5000);

        helper.retryclick(checkoutpage.addAddressButton,checkoutpage.postalCodeInput)

        checkoutpage.postalCodeInput.setValue(testdata.postalCode);
        checkoutpage.cantfindaddress.waitForEnabled(5000);
        checkoutpage.cantfindaddress.click();


        checkoutpage.firstNameInput.waitForVisible(5000);
        helper.retrysetvalue(checkoutpage.firstNameInput,testdata.firstname);
        helper.retrysetvalue(checkoutpage.lastNameInput,testdata.lastname);
        helper.retrysetvalue(checkoutpage.streetInput,testdata.street);
        helper.retrysetvalue(checkoutpage.manualPostalCodeInput,testdata.postalCode);
        helper.retrysetvalue(checkoutpage.buildingNoInput,testdata.buildingno);
        helper.retrysetvalue(checkoutpage.floorInput,testdata.floor);
        helper.retrysetvalue(checkoutpage.unitNumberInput,testdata.unit);

        helper.retryclick(checkoutpage.saveAddressButton,checkoutpage.addressNameLabel);


        checkoutpage.addressNameLabel.waitForVisible(10000);
        var name = checkoutpage.addressNameLabel.getText();
        console.log("address name :"+name);
        var index = name.indexOf(testdata.firstname+' '+testdata.lastname);
        assert.equal((index>=0),true,'Address has not saved the proper name');
        var postalCode = checkoutpage.addressPostalCodeLabel.getText();

        var postalindex = postalCode.indexOf(testdata.postalCode);
        assert.equal((postalindex>=0),true,'Address has not saved the proper postal code');

        checkoutpage.contactNumberInput.setValue(testdata.phoneno);
        checkoutpage.nextButton.click();


        //logout();
        done;

    });
});


