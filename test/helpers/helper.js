var assert = require('assert');
var sleep = require('sleep');
var testdata = require('../data/testdata');
var homepage = require('../pom/homepage');
var marketplacepage = require('../pom/marketplacepage');
var freshproducepage = require('../pom/freshproducepage');
var onsalepage = require('../pom/onsalepage');
var cartpage = require('../pom/cartpage');
var checkoutpage = require('../pom/checkoutpage');


var Helper={

    cartValue:0,
    updatedCartValue:0,

    logout:function (){
        homepage.accountDropdown();
        homepage.logoutButton.waitForEnabled(5000);
        homepage.logoutButton.click();
        homepage.loginLink.waitForExist(5000);


    },
    login:function (){
        homepage.open();
        console.log('Page loaded');
        homepage.loginLink.click();
        homepage.loginEmailaddress.setValue(testdata.username);
        homepage.loginPassword.setValue(testdata.password);
        homepage.loginSubmit.click();
    },
    addToCart: function (itemToAdd,itemPriceLabel,itemQty){
        console.log(itemPriceLabel.getText());
        itemToAdd.click();
        itemQty.waitForVisible(3000);
        homepage.viewMyCartLink.waitForVisible(5000);
        this.updatedCartValue=parseFloat(homepage.cartValueLabel.getText().slice(1));
        console.log('Current cart value '+this.updatedCartValue);
        assert.equal(this.updatedCartValue>this.cartValue,true,this.updatedCartValue+'>'+this.cartValue+'is false');
        this.cartValue=this.updatedCartValue;
    },

    removeFromCart:function (itemToRemove,itemSubtotalLabel,itemLabel){
        var itemSubtotal=parseFloat(itemSubtotalLabel.getText().slice(1))
        console.log('Removing item '+itemLabel.getText()+' of price '+itemSubtotal);
        cartValue=parseFloat(homepage.cartValueLabel.getText().slice(1));
        console.log('Current cart value '+cartValue);
        itemToRemove.click();
        homepage.viewMyCartLink.waitForVisible(5000);
        this.updatedCartValue=parseFloat(homepage.cartValueLabel.getText().slice(1));
        console.log('Current cart value '+this.updatedCartValue);
        //cartValue=(cartValue*100-itemSubtotal*100)/100;
        assert.equal(this.updatedCartValue<this.cartValue,true,this.updatedCartValue+'='+this.cartValue+' cartvalue is false');

    },

    retryclick:function (element,syncElement){
        var clicked=false;
        var i=5;
        do {
            try {
                element.waitForVisible(10000);
                element.click();
                syncElement.waitForVisible(10000);
                clicked=true;
            } catch (err) {
                console.log("caught error :"+err);

            }
        }while(!clicked && --i>0);
        return clicked;
    },

    retrysetvalue:function (element,value){
        var done=false;
        var i=5;
        do {
            try {
                element.waitForVisible(10000);
                element.scroll();
                element.click();
                element.setValue(value);
                done=true;
            } catch (err) {
                console.log("caught error :"+err);

            }
        }while(!done && --i>0);
    }

}

module.exports=Helper;
